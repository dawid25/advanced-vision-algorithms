import cv2

def block_matching(left_image, right_image):
    left_gray = cv2.cvtColor(left_image, cv2.COLOR_BGR2GRAY)
    right_gray = cv2.cvtColor(right_image, cv2.COLOR_BGR2GRAY)

    block_size = 15
    max_disparity = 64

    bm = cv2.StereoBM_create(numDisparities=max_disparity, blockSize=block_size)

    disparity_map = bm.compute(left_gray, right_gray)

    normalized_disparity_map = cv2.normalize(disparity_map, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    return normalized_disparity_map


def semi_global_matching(left_image, right_image):
    window_size = 3
    min_disparity = 0
    max_disparity = 64

    sgbm = cv2.StereoSGBM_create(minDisparity=min_disparity, numDisparities=max_disparity, blockSize=window_size)

    disparity_map = sgbm.compute(left_image, right_image)

    normalized_disparity_map = cv2.normalize(disparity_map, None, alpha=0, beta=255, norm_type=cv2.NORM_MINMAX, dtype=cv2.CV_8U)

    return normalized_disparity_map

left_image = cv2.imread('aloes/aloeL.jpg')
right_image = cv2.imread('aloes/aloeR.jpg')

# resize images
left_image = cv2.resize(left_image, (0, 0), fx=0.5, fy=0.5)
right_image = cv2.resize(right_image, (0, 0), fx=0.5, fy=0.5)

bm_disparity_map = block_matching(left_image, right_image)

sgm_disparity_map = semi_global_matching(left_image, right_image)

cv2.imshow('aloesL', left_image)
cv2.imshow('BM', bm_disparity_map)
cv2.imshow('SGM', sgm_disparity_map)
cv2.waitKey(0)
cv2.destroyAllWindows()

left_image = cv2.imread('aloes/dispL.png')
right_image = cv2.imread('aloes/dispR.png')

# resize images
left_image = cv2.resize(left_image, (0, 0), fx=0.5, fy=0.5)
right_image = cv2.resize(right_image, (0, 0), fx=0.5, fy=0.5)

bm_disparity_map = block_matching(left_image, right_image)

sgm_disparity_map = semi_global_matching(left_image, right_image)

cv2.imshow('dispL', left_image)
cv2.imshow('BM', bm_disparity_map)
cv2.imshow('SGM', sgm_disparity_map)
cv2.waitKey(0)
cv2.destroyAllWindows()
