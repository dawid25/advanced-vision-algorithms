import cv2 
import numpy as np 
import matplotlib.pyplot as plt 


#termination criteria 
criteria=(cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER,30,0.001) 
calibration_flags=cv2.fisheye.CALIB_RECOMPUTE_EXTRINSIC+cv2.fisheye.CALIB_FIX_SKEW 

#inner size of chess board 
width=9 
height=6 
square_size=0.025   #0.025meters

#prepareobjectpoints,like(0,0,0),(1,0,0),(2,0,0)....,(8,6,0) 
objp_l=np.zeros((height*width,1,3),np.float64) 
objp_l[:,0,:2]=np.mgrid[0:width,0:height].T.reshape(-1,2) 
objp_l=objp_l*square_size #Create real world coords.Use your metric. 
#Array stostore object points and image points from all the images. 
objpoints_l=[]    # 3d point in real world space 
imgpoints_l=[]    # 2d points in image plane. 
img_width_l=640 
img_height_l=480 
image_size_l=(img_width_l,img_height_l) 

image_dir="pairs" 
number_of_images=10

#left camera
for i in range(1,number_of_images): #readimage 
    img_l=cv2.imread(image_dir+"\left_%02d.png"%i) 
    gray_l=cv2.cvtColor(img_l,cv2.COLOR_BGR2GRAY) 
    #Find the chess board corners 
    ret_l,corners_l=cv2.findChessboardCorners(gray_l,(width,height),cv2. CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2. CALIB_CB_NORMALIZE_IMAGE) 
    Y_l,X_l,channels_l=img_l.shape #skip images where the corners of the chessboard are too close to the edges of the image 
    if(ret_l==True): 
        minRx_l=corners_l[:,:,0].min() 
        maxRx_l=corners_l[:,:,0].max() 
        minRy_l=corners_l[:,:,1].min() 
        maxRy_l=corners_l[:,:,1].max() 
        
        border_threshold_x_l = X_l/12 
        border_threshold_y_l = Y_l/12 
        
        x_thresh_bad_l=False 
        
        if(minRx_l<border_threshold_x_l): 
            x_thresh_bad_l=True 
        
        y_thresh_bad_l=False 
            
        if(minRy_l<border_threshold_y_l): 
            y_thresh_bad_l=True 
            
        if(y_thresh_bad_l==True) or (x_thresh_bad_l==True): 
            continue 
        
    # If found, add object points, image points (after refining them) 
    if ret_l==True: 
        objpoints_l.append(objp_l) 
        
        #improving the location of points(sub-pixel) 
        corners2_l=cv2.cornerSubPix(gray_l,corners_l,(3,3),(-1,-1), criteria)

        imgpoints_l.append(corners2_l) 
        # Draw and display the corners 
        # Show the image to see if pattern is found! imshow function. 
        cv2.drawChessboardCorners(img_l,(width,height),corners2_l,ret_l) 
        cv2.imshow("Corners left",img_l) 
        cv2.waitKey(5) 
    else: 
        print("Chessboard couldn’t detected. Left images. Image pair: ",i) 
        continue 
    
N_OK_l=len(objpoints_l) 
K_l=np.zeros((3,3)) 
D_l=np.zeros((4,1)) 
rvecs_l=[np.zeros((1,1,3),dtype=np.float64)for i in range(N_OK_l)] 
tvecs_l=[np.zeros((1,1,3),dtype=np.float64)for i in range(N_OK_l)] 

ret_l,K_l,D_l,_,_= \
cv2.fisheye.calibrate( 
    objpoints_l, 
    imgpoints_l, 
    image_size_l, 
    K_l, 
    D_l, 
    rvecs_l, 
    tvecs_l,
    calibration_flags, 
    (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER,30,1e-6) 
    ) 

# Let’s rectify our results 
map1_l,map2_l=cv2.fisheye.initUndistortRectifyMap(K_l,D_l,np.eye(3),K_l, image_size_l,cv2.CV_16SC2)

img_l = cv2.imread("pairs\left_01.png")

undistorted_image_l=cv2.remap(img_l,map1_l,map2_l,interpolation=cv2. INTER_LINEAR,borderMode=cv2.BORDER_CONSTANT)
cv2.imshow("undistorted_image_left", undistorted_image_l)
cv2.waitKey()

#right camera --------------------------------------------------------------------------------------------------------------------------------
objp_r=np.zeros((height*width,1,3),np.float64)
objp_r[:,0,:2]=np.mgrid[0:width,0:height].T.reshape(-1,2)
objp_r=objp_r*square_size #Create real world coords.Use your metric.
#Array stostore object points and image points from all the images.
objpoints_r=[]    # 3d point in real world space
imgpoints_r=[]    # 2d points in image plane.
img_width_r=640
img_height_r=480
image_size_r=(img_width_r,img_height_r)

image_dir="pairs"
number_of_images=10

for i in range(1,number_of_images): #readimage
    img_r=cv2.imread(image_dir+"/right_%02d.png"%i)
    gray_r=cv2.cvtColor(img_r,cv2.COLOR_BGR2GRAY)
    #Find the chess board corners
    ret_r,corners_r=cv2.findChessboardCorners(gray_r,(width,height),cv2. CALIB_CB_ADAPTIVE_THRESH+cv2.CALIB_CB_FAST_CHECK+cv2. CALIB_CB_NORMALIZE_IMAGE)
    Y_r,X_r,channels_r=img_r.shape #skip images where the corners of the chessboard are too close to the edges of the image
    if(ret_r==True):
        minRx_r=corners_r[:,:,0].min()
        maxRx_r=corners_r[:,:,0].max()
        minRy_r=corners_r[:,:,1].min()
        maxRy_r=corners_r[:,:,1].max()

        border_threshold_x_r = X_r/12
        border_threshold_y_r = Y_r/12

        x_thresh_bad_r=False

        if(minRx_r<border_threshold_x_r):
            x_thresh_bad_r=True

        y_thresh_bad_r=False

        if(minRy_r<border_threshold_y_r):
            y_thresh_bad_r=True

        if(y_thresh_bad_r==True) or (x_thresh_bad_r==True):
            continue

    # If found, add object points, image points (after refining them)
    if ret_r==True:
        objpoints_r.append(objp_r)

        #improving the location of points(sub-pixel)
        corners2_r=cv2.cornerSubPix(gray_r,corners_r,(3,3),(-1,-1), criteria)

        imgpoints_r.append(corners2_r)
        # Draw and display the corners
        # Show the image to see if pattern is found! imshow function.
        cv2.drawChessboardCorners(img_r,(width,height),corners2_r,ret_r)
        cv2.imshow("Corners right",img_r)
        cv2.waitKey(5)
    else:
        print("Chessboard couldn’t detected. Right images. Image pair: ",i)
        continue

N_OK_r=len(objpoints_r)
K_r=np.zeros((3,3))
D_r=np.zeros((4,1))
rvecs_r=[np.zeros((1,1,3),dtype=np.float64)for i in range(N_OK_r)]
tvecs_r=[np.zeros((1,1,3),dtype=np.float64)for i in range(N_OK_r)]

ret_r,K_r,D_r,_,_= \
cv2.fisheye.calibrate(
    objpoints_r,
    imgpoints_r,
    image_size_r,
    K_r,
    D_r,
    rvecs_r,
    tvecs_r,
    calibration_flags,
    (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER,30,1e-6)
    )

# Let’s rectify our results
map1_r,map2_r=cv2.fisheye.initUndistortRectifyMap(K_r,D_r,np.eye(3),K_r, image_size_r,cv2.CV_16SC2)

img_r = cv2.imread("pairs/right_01.png")

undistorted_image_r=cv2.remap(img_r,map1_r,map2_r,interpolation=cv2. INTER_LINEAR,borderMode=cv2.BORDER_CONSTANT)
cv2.imshow("undistorted_image_right", undistorted_image_r)
cv2.waitKey()


# dwie kamery -----------------------------------------------------------------------

imgpoints_l = np.asarray(imgpoints_l, dtype=np.float64)
imgpoints_r = np.asarray(imgpoints_r, dtype=np.float64)
(RMS, _, _, _, _, rotationMatrix, translationVector) = cv2.fisheye.stereoCalibrate(
    objpoints_l, imgpoints_l, imgpoints_r,
    K_l, D_l,
    K_r, D_r,
    image_size_l, None, None,
    cv2.CALIB_FIX_INTRINSIC,
    (cv2.TERM_CRITERIA_EPS+cv2.TERM_CRITERIA_MAX_ITER, 30, 10)
    )

R2 = np.zeros([3,3])
P1 = np.zeros([3,4])
P2 = np.zeros([3,4])
Q = np.zeros([4,4])

# Rectify calibration results
(leftRectification, rightRectification, leftProjection, rightProjection,dispartityToDepthMap) = cv2.fisheye.stereoRectify(
    K_l, D_l,
    K_r, D_r,
    image_size_l,
    rotationMatrix, translationVector,
    0, R2, P1, P2, Q,
    cv2.CALIB_ZERO_DISPARITY, (0,0) , 0, 0)

map1_left, map2_left = cv2.fisheye.initUndistortRectifyMap(
    K_l, D_l, leftRectification,
    leftProjection, image_size_l, cv2.CV_16SC2)
map1_right, map2_right = cv2.fisheye.initUndistortRectifyMap(
    K_r, D_r, rightRectification,
    rightProjection, image_size_r, cv2.CV_16SC2)

dst_L = cv2.remap(img_l, map1_left, map2_left, cv2.INTER_LINEAR)
dst_R = cv2.remap(img_r, map1_right, map2_right, cv2.INTER_LINEAR)
N, XX, YY = dst_L.shape[::-1] # RGB image size
visRectify = np.zeros((YY, XX*2, N), np.uint8) # create a new image with a new size (height, 2*width)
visRectify[:,0:XX:,:] = dst_L # left image assignment
visRectify[:,XX:XX*2:,:] = dst_R # right image assignment

# draw horizontal lines
for y in range(0,YY,10):
    cv2.line(visRectify, (0,y), (XX*2,y), (255,0,0))
cv2.imshow('visRectify',visRectify) # display image with lines
cv2.waitKey()