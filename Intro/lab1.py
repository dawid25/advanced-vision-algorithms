#import cv2
import numpy as np
import matplotlib.pyplot as plt
import cv2
from matplotlib.patches import Rectangle # add at the top of the file
import matplotlib

#load Mandril.png
I = cv2.imread("mandril.jpg")
cv2.imshow("Mandril",I) # display
cv2.waitKey(0) # wait for key
cv2.destroyAllWindows() # close all windows

cv2.imwrite("write.png",I)

print(I.shape) # dimensions /rows, columns, depth/
print(I.size) # number of bytes
print(I.dtype)

#load Mandril.png
I = cv2.imread("Mandril.jpg")


plt.figure(1) # create figure
plt.imshow(I) # add image
plt.title("Mandril DOTS") # add title
plt.axis("off") # disable display of the coordinate system

plt.imsave("write2.png",I) # save

x = [ 100, 150, 200, 250]
y = [ 50, 100, 150, 200]
plt.plot(x,y,"r.",markersize=20) # add points

fig,ax = plt.subplots(1) # instead of plt.figure(1)
rect = Rectangle((50,50),50,100,fill=False, ec="b"); # ec - edge colour
ax.add_patch(rect) # display

#show pic in gray using cv2
IG = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
cv2.imshow("Mandril Gray",IG) # display
cv2.waitKey(0) # wait for key
cv2.destroyAllWindows() # close all windows

IHSV = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)
cv2.imshow("Mandril IHSV",IHSV) # display
cv2.waitKey(0) # wait for key
cv2.destroyAllWindows() # close all windows

IH = IHSV[:,:,0]
IS = IHSV[:,:,1]
IV = IHSV[:,:,2]

print("HSV:")
print(IH)
print(IS)
print(IV)

#zad1.3
Img = plt.imread("mandril.jpg")

def rgb2gray(I):
    return 0.299*I[:,:,0] + 0.587*I[:,:,1] + 0.114*I[:,:,2]

plt.imshow(rgb2gray(Img),cmap="gray")
plt.show()

_HSV = matplotlib.colors.rgb_to_hsv(Img)
plt.imshow(_HSV[:,:,1],cmap="gray") # or _HSV[:,:,1] or _HSV[:,:,2]
plt.show()

# #zad1.4
height, width,  =I.shape[:2] # retrieving elements 1 and 2, i.e. the corresponding height and width
scale = 1.75 # scale factor
Ix2 = cv2.resize(I,(int(scale*height),int(scale*width)))
cv2.imshow("Big Mandrill",Ix2)
cv2.waitKey(0)
cv2.destroyAllWindows()

#zad1.5
Lena = cv2.imread("Lenna.png")
LenaG = cv2.cvtColor(Lena, cv2.COLOR_BGR2GRAY)
MandrilG = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)

# #add MandrilG and LenaG
Add = cv2.add(MandrilG,LenaG)
# Add = MandrilG + LenaG
cv2.imshow("Mandril + Lena",np.uint8(Add))
cv2.waitKey(0)

Sub = cv2.subtract(MandrilG,LenaG)
cv2.imshow("Mandril - Lena",np.uint8(Sub))
cv2.waitKey(0)

# #Mul = cv2.multiply(MandrilG,LenaG)
Mul = MandrilG * LenaG
cv2.imshow("Mandril * Lena",np.uint8(Mul))
cv2.waitKey(0)

# #linear combination of Mandril and lena
lc = 0.2*MandrilG + 0.5*LenaG
cv2.imshow("Linear Combination",np.uint8(lc))
cv2.waitKey(0)

# #Modulo
Mod = cv2.absdiff(MandrilG,LenaG)
cv2.imshow("Modulo",np.uint8(Mod))
cv2.waitKey(0)

#histogram
def hist(img):
    h = np.zeros((256,1), np.float32)
    height, width = img.shape[:2]
    for y in range (height):
        for x in range (width):
            h[img[y,x], 0] += 1
    return h

plt.subplot(3,1,1)
plt.plot(hist(MandrilG), label='Manual histogram')
# plt.title("Manual histogram")
# plt.legend()
# plt.show()

# #auto histogram
hist = cv2.calcHist([MandrilG],[0],None,[256],[0,256])
# [IG] -- input image
# [0] -- for greyscale images there is only one channel
# None -- mask (you can count the histogram of a selected part of the image)
# [256] -- number of histogram bins
# [0 256 ] -- the range over which the histogram is calculated

plt.subplot(3,1,2)
plt.plot(hist,color="black",linewidth=2,linestyle="-",label="Mandril",alpha=0.5,marker="o",markersize=2)
# plt.legend()
# plt.title("Auto histogram - plot")
# plt.show()

plt.subplot(3,1,3)
plt.hist(MandrilG.ravel(),256,[0,256], histtype='bar', rwidth=0.5, label="Auto histogram - hist")
plt.legend()
plt.show()

# zad1.6
IGE = cv2.equalizeHist(MandrilG)
eq_hist_img = cv2.calcHist([IGE], [0], None, [256], [0, 256])

# Wyświetlenie histogramów
plt.subplot(3, 1, 1)
plt.plot(hist)
plt.title('Histogram oryginalnego obrazu')

plt.subplot(3, 1, 2)
plt.plot(eq_hist_img)
plt.title('Histogram obrazu po wyrównaniu')



clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
# clipLimit - maximum height of the histogram bar - values above are distributed among neighbours
# tileGridSize - size of a single image block (local method, operates on separate image blocks)
I_CLAHE = clahe.apply(MandrilG)
clahe_hist_img = cv2.calcHist([I_CLAHE], [0], None, [256], [0, 256])
plt.subplot(3, 1, 3)
plt.plot(clahe_hist_img, label="CLAHE")
plt.legend()
plt.show()

Mandril = cv2.imread("Mandril.jpg")

#Gaussian, sobel, laplacian, median, gabour, library, morphology filters in one figure
#Gaussian
Gaussian = cv2.GaussianBlur(MandrilG,(5,5),0)
#Sobel
Sobel = cv2.Sobel(MandrilG,cv2.CV_64F,1,0,ksize=5)
#Laplacian
Laplacian = cv2.Laplacian(MandrilG,cv2.CV_64F)
#Median
Median = cv2.medianBlur(MandrilG,5)
#Gabor
Gabor = cv2.getGaborKernel((5,5),4,0,8,1,0,cv2.CV_64F)
Gabor = cv2.filter2D(MandrilG,-1,Gabor)

#Library
Library = cv2.bilateralFilter(MandrilG,9,75,75)

#Morphology
kernel = np.ones((5,5),np.uint8)
Morphology = cv2.morphologyEx(MandrilG, cv2.MORPH_OPEN, kernel)

#Show
plt.subplot(3, 3, 1)
plt.imshow(Gaussian, cmap='gray')
plt.title('Gaussian')

plt.subplot(3, 3, 2)
plt.imshow(Sobel, cmap='gray')
plt.title('Sobel')

plt.subplot(3, 3, 3)
plt.imshow(Laplacian, cmap='gray')
plt.title('Laplacian')

plt.subplot(3, 3, 4)
plt.imshow(Median, cmap='gray')
plt.title('Median')

plt.subplot(3, 3, 5)
plt.imshow(Gabor, cmap='gray')
plt.title('Gabor')

plt.subplot(3, 3, 6)
plt.imshow(Library, cmap='gray')
plt.title('Library')

plt.subplot(3, 3, 7)
plt.imshow(Morphology, cmap='gray')
plt.title('Morphology')

plt.subplot(3, 3, 8)
plt.imshow(MandrilG, cmap='gray')
plt.title('Original')

plt.show()
