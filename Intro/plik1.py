#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 13:42:59 2019

@author: student
"""

import cv2
import numpy as np
import matplotlib.pyplot as plt

I = cv2.imread('mandril.jpg')
cv2.imshow("Mandril", I)
cv2.waitKey(1000)
cv2.imwrite("m.png", I)
print(I.shape)
print(I.size)
print(I.dtype)

IG = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
IHSV = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)

cv2.imshow("Mandril_IG", IG)
#cv2.waitKey(1000)

cv2.imshow("Mandril_IHSV", IHSV)
#cv2.waitKey(1000)

IH = IHSV[:,:,0]
IS = IHSV[:,:,1]
IV = IHSV[:,:,2]

print(IH)
print(IS)
print(IV)

height, width = I.shape[:2]
scale = 1.75
Ix2 = cv2.resize(I,(int(scale*height), int(scale*width)))
cv2.imshow("Big_Mandril", Ix2)

J = cv2.imread('Lenna.png')
JG = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
"""SZARY = JG + IG
cv2.imshow("Dodawanko", SZARY)
#cv2.waitKey(1000)

SZ_ODJ = IG - JG
cv2.imshow("Odejmowanko", SZ_ODJ)
#cv2.waitKey(1000)

SZ_MN = IG * JG
cv2.imshow("Mnozonko", SZ_MN)
#cv2.waitKey(1000)
"""

C = 0.5 * JG + 0.75 * IG
cv2.imshow("C",np.uint8(C))
#cv2.waitKey(1000)

ABS = cv2.absdiff(JG, IG)
cv2.imshow("Modul roznicy", ABS)
#cv2.waitKey(1000)

ABS_2 = abs(JG - IG)
cv2.imshow("ABS_2", ABS_2)
cv2.waitKey()
cv2.destroyAllWindows()

def hist(img):
    h = np.zeros((256, 1), np.float32)
    height, width = img.shape[:2]
    for x in range (width):
        for y in range(height):
            h[img(x,y), 1] += 1

    return h

plt.figure()
plt.plot(hist(IG))
plt.title('Histogram 1')
plt.axis('on')
plt.show()

"""hist = cv2.calcHist([IG], [0], None, [256], [0, 256]) #druga opcja

plt.figure()
plt.subplot(1, 3, 1)
plt.hist(IG.ravel(), 256, [0, 256])
#plt.plot(hist)
#plt.plot(hist(IG))
plt.title('Histogram 1')
plt.axis('on')

plt.subplot(1, 3, 4)
cv2.imshow("IG",np.uint8(IG))
plt.axis('off')

plt.subplot(1, 3, 2)
IGE = cv2.equalizeHist(IG)
plt.hist(IGE.ravel(), 256, [0, 256])
plt.title('Histogram 2')
plt.axis('on')

plt.subplot(1, 3, 5)
cv2.imshow("IGE",np.uint8(IGE))
plt.axis('off')



plt.show()"""
