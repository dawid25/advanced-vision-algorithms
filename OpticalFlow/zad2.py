import cv2
import numpy as np
import matplotlib.pyplot as plt



def pyramid(im, max_scale):
    images = [im]
    for k in range(1, max_scale):
        images.append(cv2.resize(images[k-1], (0, 0), fx=0.5, fy=0.5))
    return images

def of(If, Jf, W2=1, dY=1, dX=1):
    # Inicjalizacja macierzy u i v
    u = np.zeros_like(If, dtype=np.float32)
    v = np.zeros_like(If, dtype=np.float32)
    # Iteracja po obrazie
    for j in range(W2+5, If.shape[0] - W2-5):
        for i in range(W2+5, If.shape[1] - W2-5):
            # Wycinanie fragmentu ramki I
            IO = np.float32(If[j - W2:j + W2 + 1, i - W2:i + W2 + 1])

            min_distance = float('inf')
            min_dx, min_dy = 0, 0

            # Przeszukiwanie otoczenia piksela J(j, i)
            for dx in range(-dX, dX + 1):
                for dy in range(-dY, dY + 1):
                    if (0 <= j + dy < Jf.shape[0]) and (0 <= i + dx < Jf.shape[1]):
                        # Wycinanie otoczenia JO
                        JO = np.float32(Jf[j + dy - W2:j + dy + W2 + 1, i + dx - W2:i + dx + W2 + 1])
                        #breakpoint()
                        # Obliczanie odległości między wycinkami IO i JO
                        distance = np.sum(np.sqrt(np.square(JO - IO)))

                        # Aktualizacja minimalnej odległości
                        if distance < min_distance:
                            min_distance = distance
                            min_dx, min_dy = dx, dy

            # Zapisywanie współrzędnych minimów w macierzach u i v
            u[j, i] = min_dx
            v[j, i] = min_dy
    return u,v

def vis_flow(u, v,I, name):
    magnitude, angle = cv2.cartToPolar(u, v)
    # Inicjalizacja obrazu w przestrzeni HSV
    hsv = np.zeros((I.shape[0], I.shape[1], 3), dtype=np.uint8)
    # Składowa H
    hsv[..., 0] = angle * 90 / np.pi
    # Składowa S (normalizacja długości wektora)
    hsv[..., 1] = cv2.normalize(magnitude, None, 0, 255, cv2.NORM_MINMAX)
    # Składowa V
    hsv[..., 2] = 255
    # Zamiana kanałów S i V
    #hsv[..., 1], hsv[..., 2] = hsv[..., 2], hsv[..., 1].copy()
    # Konwersja obrazu do przestrzeni RGB
    rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

    # Wyświetlanie obrazów
    cv2.imshow('Optical Flow', rgb)

    cv2.waitKey(0)
    cv2.destroyAllWindows()
    # # Wizualizacja pola przepływu optycznego
    # plt.figure()
    # plt.quiver(u, v)
    # plt.title(name)  # Dodaj tytuł do wykresu
    # plt.gca().invert_yaxis()
    # plt.show()

# Wczytaj obrazy I.jpg i J.jpg
I = cv2.imread("I.jpg")
J = cv2.imread("J.jpg")
# I = cv2.imread("cm1.png")
# J = cv2.imread("cm2.png")
# Konwersja obrazów do odcieni szarości
I_gray = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
J_gray = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)

# Wyświetl obrazy
#cv2.namedWindow('I', cv2.WINDOW_NORMAL)
# cv2.imshow('I', I_gray)
# #cv2.namedWindow('J', cv2.WINDOW_NORMAL)
# cv2.imshow('J', J_gray)
Diff = cv2.absdiff(I, J)
# cv2.imshow("Difference", Diff)

# cv2.waitKey(10)
# Parametry
max_scale = 3
W2 = 3
dX = dY = 3
#breakpoint()
# Wygeneruj piramidy obrazów
IP = pyramid(I_gray, max_scale)
JP = pyramid(J_gray, max_scale)

# Inicjalizacja macierzy u_total i v_total
u_total = np.zeros_like(IP[0], dtype=np.float32)
v_total = np.zeros_like(IP[0], dtype=np.float32)

I = IP[-1]
#Iteracja po skalach
for scale in range(max_scale - 1, -1, -1):

    J = JP[scale]

    # Oblicz przepływ optyczny
    u, v = of(I, J, W2, dY, dX)
    I_new = I.copy()
    for ii in range(0+W2, I_new.shape[0]-W2):
        for jj in range(0+W2, I_new.shape[1]-W2):
            zmiennaV= int(v[ii, jj])
            zmiennaU = int(u[ii,jj])
            I_new[ii,jj] = IP[scale][ii-zmiennaV,jj-zmiennaU]
    cv2.imshow('I', I)
    cv2.imshow('J', J)
    cv2.imshow('I_new', I_new)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    vis_flow(u, v, I, 'flow')
    I = cv2.resize(I_new, (0, 0),fx=2, fy=2, interpolation=cv2.INTER_LINEAR)

    #breakpoint()
    # Dodaj przepływ z bieżącej skali do sumy przepływów
    u_total += cv2.resize(u, (IP[0].shape[1], IP[0].shape[0])) * (2 ** scale)
    v_total += cv2.resize(v, (IP[0].shape[1], IP[0].shape[0])) * (2 ** scale)

# Wizualizacja całkowitego przepływu optycznego
vis_flow(u_total, v_total, IP[0], 'Total optical flow')
