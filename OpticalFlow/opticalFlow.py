import cv2
import numpy as np
import matplotlib.pyplot as plt

# read images
I = cv2.imread('materialy/I.jpg')
J = cv2.imread('materialy/J.jpg')

# resize images
I = cv2.resize(I, (0,0), fx=0.5, fy=0.5)
J = cv2.resize(J, (0,0), fx=0.5, fy=0.5)

# convert to grayscale and show
I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
cv2.imshow('IG', I)
J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
cv2.imshow('JG', J)

Diff = cv2.absdiff(I, J)
cv2.imshow('Diff', Diff)
cv2.waitKey()
cv2.destroyAllWindows()

dX = dY = W2 = 7
dist = []
dist_min = 1000

u = np.zeros((I.shape[0], I.shape[1]))
v = np.zeros((I.shape[0], I.shape[1]))

for j in range(W2+dY, I.shape[0]-W2-dY):
    for i in range(W2+dX, I.shape[1]-W2-dX):
        IO = np.float32(I[j - W2:j + W2 + 1, i - W2:i + W2 + 1])
        min_dist = 1000000
        for j_1 in range(j - dY, j + dY + 1):
            for i_1 in range(i - dX, i + dX + 1):
                JO = np.float32(J[j_1 - dY:j_1 + dY + 1, i_1 - dX:i_1 + dX + 1])
                dist = np.sum(np.sqrt((np.square(JO-IO)))) 
                if (dist < min_dist):
                    min_dist = dist
                    u[j, i] = j_1 - j
                    v[j, i] = i_1 - i
plt.quiver(u, v)
plt.gca().invert_yaxis()
plt.show()


# cart2pol
# u = np.array(u)
# v = np.array(v)f
mag, ang = cv2.cartToPolar(u, v, angleInDegrees=True)
hsv = np.zeros((I.shape[0], I.shape[1], 3), dtype=np.uint8)
hsv[..., 0] = ang
hsv[..., 1] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
hsv[..., 2] = 255
bgr = cv2.cvtColor(np.uint8(hsv), cv2.COLOR_HSV2BGR)
cv2.namedWindow("bgr", cv2.WINDOW_NORMAL)
cv2.imshow('bgr', bgr)
cv2.waitKey()
cv2.destroyAllWindows()