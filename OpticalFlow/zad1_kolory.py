import cv2
import numpy as np
import matplotlib.pyplot as plt

# Wczytaj obrazy I.jpg i J.jpg
I = cv2.imread("I.jpg")
J = cv2.imread("J.jpg")
# I = cv2.imread("cm1.png")
# J = cv2.imread("cm2.png")
# Współczynnik zmniejszenia
scale_factor = 2
# Oblicz nowe wymiary obrazu
new_width = int(I.shape[1] / scale_factor)
new_height = int(I.shape[0] / scale_factor)
# Zmniejsz obraz
I = cv2.resize(I, (new_width, new_height), interpolation=cv2.INTER_AREA)
J= cv2.resize(J, (new_width, new_height), interpolation=cv2.INTER_AREA)


# Konwersja obrazów do odcieni szarości
I_gray = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
J_gray = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)

# Wyświetl obrazy
#cv2.namedWindow('I', cv2.WINDOW_NORMAL)
cv2.imshow('I', I_gray)
#cv2.namedWindow('J', cv2.WINDOW_NORMAL)
cv2.imshow('J', J_gray)
Diff = cv2.absdiff(I, J)
cv2.imshow("Difference", Diff)

cv2.waitKey(10)
# Parametry
W2 = 5
dX = dY = 5
#breakpoint()
# Inicjalizacja macierzy u i v
u = np.zeros_like(I_gray, dtype=np.float32)
v = np.zeros_like(I_gray, dtype=np.float32)

# Iteracja po obrazie
for j in range(W2+5, I_gray.shape[0] - W2-5):
    for i in range(W2+5, I_gray.shape[1] - W2-5):
        # Wycinanie fragmentu ramki I
        IO = np.float32(I_gray[j - W2:j + W2 + 1, i - W2:i + W2 + 1])

        min_distance = float('inf')
        min_dx, min_dy = 0, 0

        # Przeszukiwanie otoczenia piksela J(j, i)
        for dx in range(-dX, dX + 1):
            for dy in range(-dY, dY + 1):
                if (0 <= j + dy < J_gray.shape[0]) and (0 <= i + dx < J_gray.shape[1]):
                    # Wycinanie otoczenia JO
                    JO = np.float32(J_gray[j + dy - W2:j + dy + W2 + 1, i + dx - W2:i + dx + W2 + 1])
                    #breakpoint()
                    # Obliczanie odległości między wycinkami IO i JO
                    distance = np.sum(np.sqrt(np.square(JO - IO)))

                    # Aktualizacja minimalnej odległości
                    if distance < min_distance:
                        min_distance = distance
                        min_dx, min_dy = dx, dy

        # Zapisywanie współrzędnych minimów w macierzach u i v
        u[j, i] = min_dx
        v[j, i] = min_dy

# Wizualizacja pola przepływu optycznego
# Konwersja przepływu optycznego na współrzędne biegunowe
magnitude, angle = cv2.cartToPolar(u, v)
# Inicjalizacja obrazu w przestrzeni HSV
hsv = np.zeros((I_gray.shape[0], I_gray.shape[1], 3), dtype=np.uint8)
# Składowa H
hsv[..., 0] = angle * 90 / np.pi
# Składowa S (normalizacja długości wektora)
hsv[..., 1] = cv2.normalize(magnitude, None, 0, 255, cv2.NORM_MINMAX)
# Składowa V
hsv[..., 2] = 255
# Zamiana kanałów S i V
hsv[..., 1], hsv[..., 2] = hsv[..., 2], hsv[..., 1].copy()
# Konwersja obrazu do przestrzeni RGB
rgb = cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)

# Wyświetlanie obrazów
cv2.imshow('Optical Flow', rgb)

cv2.waitKey(0)
cv2.destroyAllWindows()