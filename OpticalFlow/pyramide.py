import cv2
import numpy as np
import matplotlib.pyplot as plt

def pyramid(im,max_scale): 
    images=[im] 
    for k in range(1,max_scale): 
        images.append(cv2.resize(images[k-1],(0,0),fx=0.5,fy=0.5)) 
    return images

def of(I_org, I, J, W2 = 3, dY = 3, dX = 3):
    u = np.zeros((I.shape[0], I.shape[1]))
    v = np.zeros((I.shape[0], I.shape[1]))

    for j in range(W2+dY, I.shape[0]-W2-dY):
        for i in range(W2+dX, I.shape[1]-W2-dX):
            IO = np.float32(I[j - W2:j + W2 + 1, i - W2:i + W2 + 1])
            min_dist = 1000
            for j_1 in range(j - dY, j + dY + 1):
                for i_1 in range(i - dX, i + dX + 1):
                    JO = np.float32(J[j_1 - W2:j_1 + W2 + 1, i_1 - W2:i_1 + W2 + 1])
                    dist = np.sum(np.sqrt((np.square(JO-IO)))) 
                    if (dist < min_dist):
                        min_dist = dist
                        u[j, i] = j_1 - j
                        v[j, i] = i_1 - i
    return u, v
    
def vis_flow(u,v,YX,name):
    plt.quiver(u, v)
    plt.gca().invert_yaxis()
    plt.show()
    
    # cart2pol
    u = np.array(u)
    v = np.array(v)
    mag, ang = cv2.cartToPolar(u, v)
    hsv = np.zeros((YX[0], YX[1], 3))
    hsv[..., 0] = ang * 180 / np.pi / 2
    hsv[..., 1] = 255
    hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
    bgr = cv2.cvtColor(np.uint8(hsv), cv2.COLOR_HSV2BGR)
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.imshow("bgr", bgr)
    cv2.waitKey(0)
    cv2.destroyAllWindows()

# read images
I = cv2.imread('materialy/I.jpg')
J = cv2.imread('materialy/J.jpg')

# # convert to grayscale and show
# I = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
# cv2.imshow('IG', I)
# J = cv2.cvtColor(J, cv2.COLOR_BGR2GRAY)
# cv2.imshow('JG', J)

Diff = cv2.absdiff(I, J)
cv2.imshow('Diff', Diff)
cv2.waitKey(1000)
cv2.destroyAllWindows()

K = 3 # number of scales
I_pyramid = pyramid(I, K)
J_pyramid = pyramid(J, K)

u = np.zeros((I.shape[0], I.shape[1]))
v = np.zeros((I.shape[0], I.shape[1]))

for k in range(K-1, -1, -1):
    I_k = I_pyramid[k]
    J_k = J_pyramid[k]
    u = cv2.resize(u, (I_k.shape[1], I_k.shape[0]))
    v = cv2.resize(v, (I_k.shape[1], I_k.shape[0]))
    u, v = of(I, I_k, J_k)
    print(u.shape, v.shape)
    vis_flow(u, v, (I_k.shape[0], I_k.shape[1]), 'flow')



# # cart2pol
# u = np.array(u)
# v = np.array(v)
# mag, ang = cv2.cartToPolar(u, v)
# hsv = np.zeros((I.shape[0], I.shape[1], 3))
# hsv[..., 0] = ang * 180 / np.pi / 2
# hsv[..., 1] = 255
# hsv[..., 2] = cv2.normalize(mag, None, 0, 255, cv2.NORM_MINMAX)
# bgr = cv2.cvtColor(np.uint8(hsv), cv2.COLOR_HSV2BGR)
# cv2.imshow('flow', bgr)
# cv2.waitKey(0)
# cv2.destroyAllWindows()

# # convert to rgb
# rgb = cv2.cvtColor(bgr, cv2.COLOR_BGR2RGB)

# # show
# plt.imshow(rgb)
# plt.show()
